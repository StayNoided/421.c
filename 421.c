/*
 * 421.c // Un projet Open Source du 421 sur C.
 *
 *  Crée: 29 avr. 2019
 *  Fini: 27 mai 2019
 *      Auteur: StayNoided // https://gitlab.com/StayNoided/421.c
 *
 *      Défi de l'auteur: faites le système de décharge.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>

int DiceRoll(int _iMin, int _iMax){
        /* Fait un chiffre au hasard pour lancer les dés */
	    return (_iMin + (rand () % (_iMax-_iMin+1)));
}
void sleep(unsigned long int n) {
        /* boucle vide parcourue (n * 100000) fois*/
        int i = 0;
        unsigned long int max = n * 100000;
        do {
                /* Faire qqch de stupide qui prend du temps */
                i++;
        }
        while(i <= max);
}

int DiceClass(int dP1,int dP2,int dP3){
    /* Classe les dés par ordre décroissant */
	int sum, voidNum;
	if(dP1<dP2){
		voidNum=dP1;
		dP1=dP2;
		dP2=voidNum;

	}
	if(dP1<dP3){
			voidNum=dP1;
			dP1=dP3;
			dP3=voidNum;

		}
	if(dP2<dP3){
			voidNum=dP2;
			dP2=dP3;
			dP3=voidNum;

		}
	sum=(dP1*100)+(dP2*10)+dP3;
 return sum;
}
void DiceAff(int diceValue){
    /* Affiche les dés */
	 switch(diceValue)
	    {
	        case 1 :
	        printf(" +---------+\n");
	        printf(" |         |\n");
	        printf(" |         |\n");
	        printf(" |    O    |\n");
	        printf(" |         |\n");
	        printf(" |         |\n");
	        printf(" +---------+\n");
	        break;
	        case 2 :
	        printf(" +---------+\n");
	        printf(" | O       |\n");
	        printf(" |         |\n");
	        printf(" |         |\n");
	        printf(" |         |\n");
	        printf(" |       O |\n");
	        printf(" +---------+\n");
	        break;
	        case 3 :
	        printf(" +---------+\n");
	        printf(" | O       |\n");
	        printf(" |         |\n");
	        printf(" |    O    |\n");
	        printf(" |         |\n");
	        printf(" |       O |\n");
	        printf(" +---------+\n");
	        break;
	        case 4 :
	        printf(" +---------+\n");
	        printf(" | O     O |\n");
	        printf(" |         |\n");
	        printf(" |         |\n");
	        printf(" |         |\n");
	        printf(" | O     O |\n");
	        printf(" +---------+\n");
	        break;
	        case 5 :
	        printf(" +---------+\n");
	        printf(" | O     O |\n");
	        printf(" |         |\n");
	        printf(" |    O    |\n");
	        printf(" |         |\n");
	        printf(" | O     O |\n");
	        printf(" +---------+\n");
	        break;
	        case 6 :
	        printf(" +---------+\n");
	        printf(" | O     O |\n");
	        printf(" |         |\n");
	        printf(" | O     O |\n");
	        printf(" |         |\n");
	        printf(" | O     O |\n");
	        printf(" +---------+\n");
	        break;

	    }

}

int crtn(int Valeur){
    /* Affecte les cartons aux exceptions(2-8 cartons) et aux autres combinaisons(1 carton) */
	int Cartons;
	 switch(Valeur)
	    {
	        case 421 :
	        	Cartons=8;
	        break;
	        case 111:
	        	Cartons=7;
	        break;
	        case 611:
	        	Cartons=6;
	        break;
	        case 511 :
	        	Cartons=5;
	        break;
	        case 411 :
	        	Cartons=4;
	        break;
	        case 311 :
	            Cartons=3;
	        break;
	        case 666 :
	            Cartons=3;
	        break;
	        case 555 :
	            Cartons=3;
	        break;
	        case 444 :
	            Cartons=3;
	        break;
	        case 333 :
                Cartons=3;
	        break;
	        case 222 :
	        	Cartons=3;
	        break;
	        case 211 :
	            Cartons=2;
	        break;
	        case 654 :
	            Cartons=2;
	        break;
	        case 543 :
	            Cartons=2;
	        break;
	        case 432 :
	            Cartons=2;
	        break;
	        case 321 :
	        	Cartons=2;
	        break;
	        default:
	        	Cartons=1;
	        	break;
	    }
	 return Cartons;
	 /* Ceci attribue le nb de Cartons
	  *
	  */
}
int cmpr(int Valeur){
    /* Ceci affecte une valeur aux exceptions pour qu'elles soit + grandes que 665| Nmb de remplacement pour la comparaison et affecter les pts*/
	int Cartons;
	 switch(Valeur)
	    {
	        case 421 :
	        	Cartons=999;
	        break;
	        case 111:
	        	Cartons=998;
	        break;
	        case 611:
	        	Cartons=997;
	        break;
	        case 511 :
	        	Cartons=996;
	        break;
	        case 411 :
	        	Cartons=995;
	        break;
	        case 311 :
	            Cartons=994;
	        break;
	        case 666 :
	            Cartons=993;
	        break;
	        case 555 :
	            Cartons=992;
	        break;
	        case 444 :
	            Cartons=991;
	        break;
	        case 333 :
                Cartons=990;
	        break;
	        case 222 :
	        	Cartons=989;
	        break;
	        case 211 :
	            Cartons=987;
	        break;
	        case 654 :
	            Cartons=986;
	        break;
	        case 543 :
	            Cartons=985;
	        break;
	        case 432 :
	            Cartons=984;
	        break;
	        case 321 :
	        	Cartons=983;
	        break;
	        default:
	        	Cartons=Valeur;
	        	break;
	    }
	 return Cartons;
	 /* Ceci attribue la valeur
	  *
	  */
}

void main()
{
    int cartonsP, cartonsO, nbCartons, menuChoice, waitCycle, onze;
	int dP1, dP2, dP3, stockP,ValeurP, totalP; /* ints Joueurs */
	int dO1, dO2, dO3, ValeurO, stockO, totalO; /* Ordis */
	int c,d,u, parties, cont, Valeur;
	char nbManches;
    int stime;
    printf("Bienvenue au:\n");
    printf(" _  _  ____  _ \n");
    printf("| || ||___ ./ |\n");
    printf("| || |_ __) | |\n");
    printf("|__   _/ __/| |\n");
    printf("   |_||_____|_|\n");
    printf("Edition Solitude Maxi\n\n");
    printf("Ceci est un projet scolaire de fin d'annee qui est disponible\nen open-source sur https://gitlab.com/StayNoided/421.c\n\n\n");
    printf("\nAppuyez sur [ENTREE] pour demarrer la partie\n\n\n\n");
    waitCycle=0;
    getch();
    cartonsO = 0;
    cartonsP = 0;
    stockO = 0;
    stockP = 0;
    totalO = 0;
    totalP = 0;
    parties=0;
    cont=0;
    onze=11;
    while (cont<=1&&cont>-1){ /* verifie si continue ou pas */

    printf("Le joueur lance les des: [ENTREE] pour lancer\n");
    srand(time(NULL));
    dP1 = DiceRoll(1,6);
    dP2 = DiceRoll(1,6);
    dP3 = DiceRoll(1,6);
    Valeur = DiceClass(dP1,dP2,dP3);
    totalP = crtn(Valeur);
    ValeurP = cmpr(Valeur);
    getch();

    c = (Valeur%1000-Valeur%100)/100;
    d = (Valeur%100-Valeur%10)/10;
    u = Valeur%10;
    DiceAff(c);
    getch();
    DiceAff(d);
    getch();
    DiceAff(u);
    printf("\n     %d\n\nL'ORDI lance les des: Attendez quelques secondes\n",Valeur);
    sleep(1000);
    printf("\n\n");
    srand(time(NULL));
    dO1 = DiceRoll(1,6);
    sleep((DiceRoll(1,5)*1100));
    dO2 = DiceRoll(1,6);
    sleep((DiceRoll(1,5)*1100));
    dO3 = DiceRoll(1,6);
    Valeur = DiceClass(dO1,dO2,dO3);
    totalO = crtn(Valeur);
    ValeurO = cmpr(Valeur);
    c = (Valeur%1000-Valeur%100)/100;
    d = (Valeur%100-Valeur%10)/10;
    u = Valeur%10;
    DiceAff(c);
    DiceAff(d);
    DiceAff(u);
    printf("\n\n    %d\n\n",Valeur);



    if(totalP==totalO){
            /* Au cas ou les memes nb de cartons sont attribués, verif quel est le nb le plus haut */
    if(ValeurP>ValeurO){
        cartonsO = cartonsO+totalP;
        onze = onze-totalP;
        if(onze<=0){
            cartonsO = cartonsO-onze;
        }

    }
    else if(ValeurP<ValeurO){
        cartonsP = cartonsP+totalO;
        onze = onze-totalO;
        if(onze<=0){
        cartonsP = cartonsP-onze;
        }
    }

    else if(ValeurP==ValeurO){
        printf("\nEgalite: 0 cartons ou pat distribues.\n\n");
    }
    }
    else if(totalP!=totalO){ /* Verifie qui attribue le plus de cartons */
        if(ValeurP>ValeurO){
        cartonsO = cartonsO+totalP;
        onze = onze-totalP;
        if(onze<=0){
            cartonsO = cartonsO-onze;
        }
        }
        if(ValeurP<ValeurO){
        cartonsP = cartonsP+totalO;
        onze = onze-totalO;
        if(onze<=0){
        cartonsP = cartonsP-onze;
        }
        }
    }
    if ((cartonsP+cartonsO)>=11){
    if (cartonsP>cartonsO){
        stockP++;
        cartonsP = 0;
        cartonsO = 0;
        onze=11;
        printf("+1 pat pour le joueur\n\n");

    }
    else{
        stockO++;
        cartonsO = 0;
        cartonsP = 0;
        onze=11;
        printf("\n+1 pat pour l'ordi\n\n");

    }
    }
    printf("Nombres de cartons du Joueur:%d\n\nNombres de cartons de l'ORDI: %d\n\nNombres de cartons restants: %d\n\n",cartonsP, cartonsO, onze);

    printf("\nVous avez un total de %d pat",stockP);
    if (stockP>=0){
        printf("\n\n");
    }
        else{
        printf("s\n\n");
    }

    printf("L'ordi a un total de %d pat\n",stockO);
    if (stockO>=0){
        printf("\n\n");
    }
        else{
        printf("s\n\n");
    }




    printf("\n\nContinuer?\n\n Oui <0>, Oui & [REMISE A ZERO] <1> ou Non <2>\n");
    scanf("%d",&cont);
    if(cont<0||cont>2){
    while(cont<0||cont>2){
    printf("\n\nRequete invalide, veuillez repondre par oui ou par non.\n\n");
    printf("Continuer?\n\n Oui <0>, Oui & [REMISE A ZERO] <1> ou Non <2>");
    scanf("%d",&cont);
    }
    }
    if(cont==1){
        stockP=0;
        stockO=0;
        cartonsP=0;
        cartonsO=0;
        onze=11;
    }
    printf("\n\n\n");


    }
    /* Scoreboard de fin. */
    printf("\nJoueur: %d pat \n",stockP);
    printf("\nOrdi: %d pat \n",stockO);
    if (stockP<stockO){
        printf("\nLe Joueur a perdu avec %d pats \n\n",stockP);
    }
    else if (stockP>stockO){

        printf("\nL'Ordinateur a perdu avec %d pats \n\n",stockO);
    } else if (stockP=stockO){
    printf("\nEgalite: Vous et l'ORDI avez %d pats \n\n",stockO);
    }



}